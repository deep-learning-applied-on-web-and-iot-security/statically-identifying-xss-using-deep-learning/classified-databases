<!--
Unsafe sample
input : use proc_open to read /tmp/tainted.txt
sanitize : use of ternary condition
File : unsafe, use of untrusted data in a property value in a span tag(CSS)
<!--



<!--
MIT License

Copyright (c) 2021 MAUREL Héloïse

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-->


<!DOCTYPE html>
<html>
<head>
<title> XSS </title>
</head>
<body>
<?php
$descriptorspec = array(
      0 => array("pipe", "r"),
      1 => array("pipe", "w"),
      2 => array("file", "/tmp/error-output.txt", "a")
      );
      $cwd = '/tmp';
      $process = proc_open('more /tmp/tainted.txt', $descriptorspec, $pipes, $cwd, NULL);
      if (is_resource($process)) {
      fclose($pipes[0]);
      $tainted = stream_get_contents($pipes[1]);
      fclose($pipes[1]);
      $return_value = proc_close($process);
      }

$tainted = $tainted  < 10.5 ?$tainted : 'safe2';

	//flaw

echo "<a href=". $tainted .">link</a >" ;
?>
<h1>Hello World!</h1>
</body>
</html>
