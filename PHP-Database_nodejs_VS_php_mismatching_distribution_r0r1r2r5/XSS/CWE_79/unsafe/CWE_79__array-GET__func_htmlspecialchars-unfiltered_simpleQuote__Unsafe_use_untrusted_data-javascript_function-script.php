<!--
Unsafe sample
input : get the $_GET['userData'] in an array
sanitize : use of the function htmlspecialchars. Sanitizes the query but has a high chance to produce unexpected results
File : unsafe, use of untrusted data in a function in a script
<!--



<!--
MIT License

Copyright (c) 2021 MAUREL Héloïse

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-->


<!DOCTYPE html>
<html>
<head>
    <title> XSS </title>
</head>
<script>
function xss()
{
<?php
echo "<!DOCTYPE html><html><head><title> XSS </title></head><script>function xss(){" ;
$array = array();
      $array[] = 'safe' ;
      $array[] = $_GET['userData'] ;
      $array[] = 'safe' ;
      $tainted = $array[1] ;

$tainted = htmlspecialchars($tainted);

	//flaw

echo $tainted ."}" ;

?>
</script>
</head>
<body onload="xss()">
<h1>Hello World!</h1>
</body>
</html>